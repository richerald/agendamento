FROM openjdk:11-jdk
VOLUME /tmp
COPY ./target/agendamento-0.0.1-SNAPSHOT.jar agendamento.jar
ENTRYPOINT ["java","-jar","/agendamento.jar"]
# Introdução

## Tecnologias utilizadas
- Spring boot
- Java 11
- Mysql 8
- Redis

## Contexto

A API é organizada nos contextos abaixo:

### Médicos

- Obter agendamentos do médico
> Esse recurso retorna informações do médico e uma lista de agendamentos dos pacientes. Caso nao exista nenhum agendamento a lista voltara vazia.

### Agendamentos

- Criar agendamento
> Esse recurso agenda uma consulta para o paciente. Existem algumas situações em que o agendamento não será salvo:
> 
> 1 - O paciente não exista na base de dados.
> 
> 2 - A data do agendamento seja menor que a data atual.
> 
> 3 - Foi considerado que um médico demora `30 minutos` para atender um paciente. Logo um agendamento so poderá ser salvo 
> caso o médico e o paciente estejam fora do intervalo de 30 minutos de outro agendamento existente.

### Pacientes

- Listar pacientes
> Esse recurso retorna uma lista com os pacientes. Caso não existam pacientes cadastrados, será retornada uma lista vazia.

- Pesquisar paciente
> Esse recurso pesquisa um paciente pelo seu ID. Será retornado erro caso o paciente não exista.

- Criar paciente
> Esse recurso cria paciente. Existem algumas situações em que o paciente nao sera salvo:
> 
> 1 - CPF já exista na base de dados
> 
> 2 - Exista alguma inconsistência nas informações passadas.

- Atualizar paciente
> Esse recurso atualiza nome, idade e telefone do paciente. Existem algumas situações em que o paciente não sera atualizado:
>
> 1 - O paciente não exista na base de dados.
> 
> 2 - Exista alguma inconsistência nas informações passadas.

- Remover paciente
> Esse recurso remove o paciente da base de dados. Existem algumas situações em que o paciente não sera removido:
>
> 1 - O paciente não exista na base de dados.
> 
> 2 - Exista agendamentos vinculados ao paciente.

### Logout

- Logout
> Esse recurso revoga o token do usuário logado por 1 dia.

## Configurações

Para executar a aplicação siga os passos abaixo:

- Antes de tudo devemos subir o banco de dados, certifique-se que esta na raiz do projeto e execute `docker-compose up -d mysql redis`
- Agora precisamos criar uma imagem da aplicação, para isso temos que executar alguns passos:
    - `./mvnw clean package -DskipTests`
    - `docker build -t agendamento:v1 .`
- por último subimos a aplicação com o comando `docker-compose up agendamento`

Existe no projeto um Makefile que simplifica os passos acima.

- Para iniciar a aplicação
> `make up`

- Para desligar a aplicação
> `make down`

Agora precisamos importar os dois arquivos do postman que estão na raiz do projeto. Um arquivo contém a coleção de requisições e o outro, as variáveis de ambiente.

A aplicação por padrão cria um paciente e dois médicos. Segue abaixo dados para utilização:

> paciente id = 1

> usuario: richard.herald
> 
> senha: 12345678
 
> usuario: eraldo.neto
> 
> senha: 12345678
 
Caso queira executar a aplicação utilizando a IDE é necessário acessar o arquivo `application.yml` e trocar as conexões dos bancos de dados redis e mysql por localhost.
Também existe a opção de adicionar no seu arquivo hosts dois redirecionamentos:

- `127.0.0.1    redis`
- `127.0.0.1    mysql`

Caso queria acessar a documentação da aplicação, utilize o link http://localhost:8081/swagger-ui.html#/.

É necessário estar autenticado para utilizar todos os recursos da documentação. Observe que existe um botão chamado Authorize, clique nele e utilize os dados abaixo:

`username` e `password` sao os dados de `login` do médico que foram passados acima.

`client_id = appclient`

`client_secret = 12345678`
package br.com.agendamento.service;

import br.com.agendamento.domain.Medico;
import br.com.agendamento.dto.MedicoDTO;

import java.util.Optional;

public interface MedicoService {

    String getAgendamentosById(Integer id);

    MedicoDTO getById(Integer id);

    Optional<Medico> getByUsuario(String usuario);
}

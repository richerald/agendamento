package br.com.agendamento.service;

import br.com.agendamento.domain.Medico;
import br.com.agendamento.dto.MedicoDTO;
import br.com.agendamento.exceptionhandler.BusinessException;
import br.com.agendamento.mapper.MedicoMapper;
import br.com.agendamento.repository.MedicoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class MedicoServiceImpl implements MedicoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicoServiceImpl.class);

    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;
    private final MedicoResolver medicoResolverImpl;

    public MedicoServiceImpl(MedicoRepository medicoRepository,
                             MedicoMapper medicoMapper,
                             MedicoResolver medicoResolverImpl) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
        this.medicoResolverImpl = medicoResolverImpl;
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getAgendamentosById(Integer id) {

        medicoResolverImpl.isValidMedico(id);

        getById(id);

        return medicoRepository.getAgendamentosById(id)
                .orElse("{}");
    }

    public MedicoDTO getById(Integer id) {
        Medico medico = medicoRepository.getById(id)
                .orElseThrow(() -> {
                    LOGGER.error("Medico com id {} não existe", id);
                    return new BusinessException(HttpStatus.NOT_FOUND, "medico.not-found");
                });

        return medicoMapper.toDTO(medico);
    }

    public Optional<Medico> getByUsuario(String usuario) {
        return medicoRepository.getByUsuario(usuario);
    }
}

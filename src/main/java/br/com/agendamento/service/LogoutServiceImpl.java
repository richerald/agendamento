package br.com.agendamento.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class LogoutServiceImpl implements LogoutService {

    private final RedisService redisService;

    @Autowired
    public LogoutServiceImpl(RedisService redisService) {
        this.redisService = redisService;
    }

    public void logout(HttpServletRequest request) {
        String token = request.getHeader("Authorization").split("Bearer ")[1];
        redisService.addBlacklist(token);
    }
}

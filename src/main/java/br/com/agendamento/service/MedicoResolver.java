package br.com.agendamento.service;

public interface MedicoResolver {

    Integer getMedicoId() throws SecurityException;

    void isValidMedico(Integer medicoId) throws SecurityException;
}

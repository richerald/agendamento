package br.com.agendamento.service;

public interface RedisService {

    void addBlacklist(String token);

    Boolean existsTokenInBlacklist(String token);
}

package br.com.agendamento.service;

import br.com.agendamento.dto.PacienteDTO;
import br.com.agendamento.exceptionhandler.BusinessException;
import br.com.agendamento.mapper.PacienteMapper;
import br.com.agendamento.repository.PacienteRepository;
import br.com.agendamento.domain.Paciente;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PacienteServiceImpl implements PacienteService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PacienteServiceImpl.class);

    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;

    @Autowired
    public PacienteServiceImpl(PacienteRepository pacienteRepository,
                               PacienteMapper pacienteMapper) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public void delete(Integer pacienteId) {

        getById(pacienteId);

        if (existsAgendamentoByPacienteId(pacienteId)) {
            LOGGER.error("Nao e possivel deletar o paciente com id {}, pois existem agendamentos vinculados a ele", pacienteId);
            throw new BusinessException("paciente.exists-agendamentos");
        }

        if (!pacienteRepository.delete(pacienteId)) {
            LOGGER.error("Paciente com id {} não foi deletado", pacienteId);
            throw new BusinessException("generic-error-2");
        }
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public PacienteDTO update(PacienteDTO pacienteDTO) {

        getById(pacienteDTO.getId());

        Paciente paciente = pacienteMapper.toEntity(pacienteDTO);

        if (!pacienteRepository.update(paciente)) {
            LOGGER.error("Paciente com id {} não foi atualizado", pacienteDTO.getId());
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR, "generic-error-2");
        }

        return pacienteDTO;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public PacienteDTO save(PacienteDTO pacienteDTO) {

        if (existsByCPF(pacienteDTO.getCpf())) {
            LOGGER.error("Paciente com CPF {} ja esta cadastrado na base de dados", pacienteDTO.getCpf());
            throw new BusinessException("paciente.already-exists");
        }

        Paciente paciente = pacienteMapper.toEntity(pacienteDTO);
        paciente = pacienteRepository.save(paciente);

        pacienteDTO.setId(paciente.getId());
        return pacienteDTO;
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public String getAll() {
        return pacienteRepository.getAll()
                .orElse("[]");
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public PacienteDTO getById(Integer pacienteId) {

        Paciente paciente = pacienteRepository.getById(pacienteId)
                .orElseThrow(() -> {
                    LOGGER.error("Paciente com id {} não existe", pacienteId);
                    return new BusinessException(HttpStatus.NOT_FOUND, "paciente.not-found");
                });

        return pacienteMapper.toDTO(paciente);
    }

    public boolean existsByCPF(String CPF) {
        return pacienteRepository.existsByCPF(CPF);
    }

    public boolean existsAgendamentoByPacienteId(Integer pacienteId) {
        return pacienteRepository.existsAgendamentoByPacienteId(pacienteId);
    }
}

package br.com.agendamento.service;

import br.com.agendamento.domain.Agendamento;
import br.com.agendamento.dto.AgendamentoDTO;
import br.com.agendamento.exceptionhandler.BusinessException;
import br.com.agendamento.mapper.AgendamentoMapper;
import br.com.agendamento.repository.AgendamentoRepository;
import br.com.agendamento.utils.LocalDateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AgendamentoServiceImpl implements AgendamentoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AgendamentoServiceImpl.class);

    private final PacienteService pacienteService;
    private final MedicoService medicoService;
    private final AgendamentoMapper agendamentoMapper;
    private final AgendamentoRepository agendamentoRepository;
    private final MedicoResolver medicoResolver;
    private final LocalDateTimeUtils localDateTimeUtils;

    @Autowired
    public AgendamentoServiceImpl(PacienteService pacienteService,
                                  MedicoService medicoService,
                                  AgendamentoMapper agendamentoMapper,
                                  AgendamentoRepository agendamentoRepository,
                                  MedicoResolver medicoResolver,
                                  LocalDateTimeUtils localDateTimeUtils) {
        this.pacienteService = pacienteService;
        this.medicoService = medicoService;
        this.agendamentoMapper = agendamentoMapper;
        this.agendamentoRepository = agendamentoRepository;
        this.medicoResolver = medicoResolver;
        this.localDateTimeUtils = localDateTimeUtils;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    public AgendamentoDTO save(AgendamentoDTO agendamentoDTO) {

        if (localDateTimeUtils.isAfterNow(agendamentoDTO.getAgendamentoData())) {
            LOGGER.error("Nao e possivel agendar um atendimento para uma data no passado");
            throw new BusinessException("agendamento.agendamento-data-in-the-past");
        }

        agendamentoDTO.setMedicoId(medicoResolver.getMedicoId());

        pacienteService.getById(agendamentoDTO.getPacienteId());
        medicoService.getById(agendamentoDTO.getMedicoId());

        if (agendamentoRepository.isHorarioIndisponivelPaciente(agendamentoDTO.getAgendamentoData(), agendamentoDTO.getPacienteId())) {
            LOGGER.error("O paciente ID {} esta com alguma consulta agendada nesse horario", agendamentoDTO.getPacienteId());
            throw new BusinessException("agendamento.paciente-no-time-available");
        }

        if (agendamentoRepository.isHorarioIndisponivelMedico(agendamentoDTO.getAgendamentoData(), agendamentoDTO.getMedicoId())) {
            LOGGER.error("O medico ID {} esta com alguma consulta agendada nesse horario", agendamentoDTO.getMedicoId());
            throw new BusinessException("agendamento.medico-no-time-available");
        }

        Agendamento agendamento = agendamentoMapper.toEntity(agendamentoDTO);
        agendamento = agendamentoRepository.save(agendamento);

        agendamentoDTO.setId(agendamento.getId());
        return agendamentoDTO;
    }
}

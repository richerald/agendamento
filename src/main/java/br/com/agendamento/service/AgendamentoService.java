package br.com.agendamento.service;

import br.com.agendamento.dto.AgendamentoDTO;

public interface AgendamentoService {

    AgendamentoDTO save(AgendamentoDTO agendamentoDTO);
}

package br.com.agendamento.service;

import br.com.agendamento.dto.PacienteDTO;
import org.springframework.stereotype.Service;

@Service
public interface PacienteService {

    void delete(Integer pacienteId);

    PacienteDTO update(PacienteDTO pacienteDTO);

    PacienteDTO save(PacienteDTO pacienteDTO);

    String getAll();

    PacienteDTO getById(Integer pacienteId);

    boolean existsByCPF(String CPF);

    boolean existsAgendamentoByPacienteId(Integer pacienteId);

}

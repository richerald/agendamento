package br.com.agendamento.service;

import br.com.agendamento.exceptionhandler.BusinessException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Service;

import static java.util.Objects.nonNull;

@Service
public class MedicoResolverImpl implements MedicoResolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicoResolverImpl.class);
    private static final String ANONYMOUS_USER = "anonymoususer";

    @Override
    public Integer getMedicoId() throws SecurityException {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String principal = (String) authentication.getPrincipal();

            if (principal.equalsIgnoreCase(ANONYMOUS_USER)) return null;

            OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) authentication.getDetails();
            Jwt jwt = JwtHelper.decode(details.getTokenValue());

            return (Integer) new JSONObject(jwt.getClaims()).get("medico_identifier");
        } catch (Exception e) {
            LOGGER.error("Não foi possível extrair o id do medico logado");
            LOGGER.error("Erro: " + e.getMessage());
            return null;
        }
    }

    @Override
    public void isValidMedico(Integer medicoId) {

        Integer medicoIdToken = getMedicoId();

        if (nonNull(medicoIdToken) && !medicoIdToken.equals(medicoId)) {
            LOGGER.error("O medico logado com id {} é diferente do medico passado por parametro com id {}", medicoIdToken, medicoId);
            throw new BusinessException("medico.id-invalid");
        }
    }
}

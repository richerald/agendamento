package br.com.agendamento.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class RedisServiceImpl implements RedisService {

    private final RedisTemplate<String, String> redisTemplate;

    @Autowired
    public RedisServiceImpl(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public void addBlacklist(String token) {
        redisTemplate.opsForValue().set(token, token);
        redisTemplate.expire(token, 24, TimeUnit.HOURS);
    }

    public Boolean existsTokenInBlacklist(String token) {
        return redisTemplate.hasKey(token);
    }
}

package br.com.agendamento.controller;

import br.com.agendamento.service.MedicoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/app/v1")
@Api(tags = "Medicos")
public class MedicoController {

    private final MedicoService medicoService;

    @Autowired
    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

    @ApiOperation(value = "Recurso responsável listar todos os agendamentos do medico logado")
    @GetMapping(value = "medicos/{id}/agendamentos", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PreAuthorize("isFullyAuthenticated() and #oauth2.hasAnyScope('appscope')")
    public ResponseEntity<String> getAgendamentosById(@PathVariable Integer id) {
        return new ResponseEntity<>(medicoService.getAgendamentosById(id), HttpStatus.OK);
    }
}

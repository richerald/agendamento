package br.com.agendamento.controller;

import br.com.agendamento.service.LogoutService;
import br.com.agendamento.service.LogoutServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/app/v1")
@Api(tags = "Logout")
public class LogoutController {

    private final LogoutService logoutService;

    @Autowired
    public LogoutController(LogoutServiceImpl logoutService) {
        this.logoutService = logoutService;
    }

    @ApiOperation(value = "Recurso responsável por revogar o token do usuario logado")
    @GetMapping(value = "/logout")
    @PreAuthorize("isFullyAuthenticated() and #oauth2.hasAnyScope('appscope')")
    public ResponseEntity<Void> logout(HttpServletRequest request) {
        logoutService.logout(request);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

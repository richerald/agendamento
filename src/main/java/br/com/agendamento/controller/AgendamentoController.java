package br.com.agendamento.controller;

import br.com.agendamento.dto.AgendamentoDTO;
import br.com.agendamento.service.AgendamentoService;
import br.com.agendamento.service.AgendamentoServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/app/v1")
@Api( tags = "Agendamentos")
public class AgendamentoController {

    private final AgendamentoService agendamentoService;

    @Autowired
    public AgendamentoController(AgendamentoServiceImpl agendamentoService) {
        this.agendamentoService = agendamentoService;
    }

    @ApiOperation(value = "Recurso responsável por agendar uma consulta")
    @PostMapping(value = "/agendamentos")
    @PreAuthorize("isFullyAuthenticated() and #oauth2.hasAnyScope('appscope')")
    public ResponseEntity<AgendamentoDTO> save(@Valid @RequestBody AgendamentoDTO agendamentoDTO) {
        return new ResponseEntity<>(agendamentoService.save(agendamentoDTO), HttpStatus.CREATED);
    }
}

package br.com.agendamento.controller;

import br.com.agendamento.dto.PacienteDTO;
import br.com.agendamento.service.PacienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/app/v1")
@Api(tags = "Pacientes")
public class PacienteController {

    private final PacienteService pacienteService;

    @Autowired
    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }

    @ApiOperation(value = "Recurso responsável por listar todos os pacientes")
    @GetMapping(value = "/pacientes", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getAll() {
        return new ResponseEntity<>(pacienteService.getAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Recurso responsável por pesquisar um paciente pelo id")
    @GetMapping(value = "/pacientes/{id}")
    public ResponseEntity<PacienteDTO> getById(@PathVariable Integer id) {
        return new ResponseEntity<>(pacienteService.getById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Recurso responsável inserir um novo paciente")
    @PostMapping(value = "/pacientes")
    public ResponseEntity<PacienteDTO> save(@Valid @RequestBody PacienteDTO pacienteDTO) {
        return new ResponseEntity<>(pacienteService.save(pacienteDTO), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Recurso responsável deletar um paciente")
    @DeleteMapping(value = "pacientes/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
        pacienteService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @ApiOperation(value = "Recurso responsável atualizar os dados do paciente")
    @PutMapping(value = "pacientes/{id}")
    public ResponseEntity<PacienteDTO> update(@Valid @RequestBody PacienteDTO pacienteDTO, @PathVariable Integer id) {
        pacienteDTO.setId(id);
        return new ResponseEntity<>(pacienteService.update(pacienteDTO), HttpStatus.OK);
    }
}

package br.com.agendamento.config.security.refreshtoken;

import br.com.agendamento.config.property.EnableHttpsProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import static java.util.Objects.nonNull;

@ControllerAdvice
public class RefreshTokenPostProcessor implements ResponseBodyAdvice<OAuth2AccessToken> {

    @Value("${agendamento.utils.urls.custom-token-path}")
    private String customTokenPath;

    private final EnableHttpsProperty enableHttpsProperty;

    @Autowired
    public RefreshTokenPostProcessor(EnableHttpsProperty enableHttpsProperty) {
        this.enableHttpsProperty = enableHttpsProperty;
    }

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        return returnType.getMethod().getName().equals("postAccessToken");
    }

    @Override
    public OAuth2AccessToken beforeBodyWrite(OAuth2AccessToken body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {

        HttpServletResponse resp = ((ServletServerHttpResponse) response).getServletResponse();

        DefaultOAuth2AccessToken token = (DefaultOAuth2AccessToken) body;

        if (nonNull(body) && nonNull(body.getRefreshToken())) {
            String refreshToken = body.getRefreshToken().getValue();
            addRefreshTokenNoCookie(refreshToken, resp);
            removeRefreshTokenBody(token);
        }

        return body;
    }

    private void removeRefreshTokenBody(DefaultOAuth2AccessToken token) {
        token.setRefreshToken(null);
    }

    private void addRefreshTokenNoCookie(String refreshToken, HttpServletResponse resp) {
        Cookie refreshTokenCookie = new Cookie("refreshToken", refreshToken);
        refreshTokenCookie.setHttpOnly(true);
        refreshTokenCookie.setSecure(enableHttpsProperty.isEnableHttps());
        refreshTokenCookie.setPath(customTokenPath);
        refreshTokenCookie.setMaxAge(3_000_000);
        resp.addCookie(refreshTokenCookie);
    }
}

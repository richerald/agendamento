package br.com.agendamento.config.security;

import br.com.agendamento.domain.Medico;
import br.com.agendamento.domain.SystemMedico;
import br.com.agendamento.service.MedicoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.*;

import static java.util.Objects.nonNull;

@Service
public class AgendamentoUserDetailsService implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AgendamentoUserDetailsService.class);

    private final MedicoService medicoService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AgendamentoUserDetailsService(MedicoService medicoService,
                                         PasswordEncoder passwordEncoder) {
        this.medicoService = medicoService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String usuario) throws UsernameNotFoundException {

        String password = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getParameter("password");

        Medico medico = medicoService.getByUsuario(usuario)
                .orElseThrow(() -> {
                    LOGGER.error("Autenticacao: o usuário {} está inválido", usuario);
                    throw new UsernameNotFoundException("Usuário e/ou senha inválido(s)");
                });

        if (nonNull(password) && !passwordEncoder.matches(password, medico.getSenha())) {
            LOGGER.error("Autenticacao: a senha do usuário {} está inválida", usuario);
            throw new UsernameNotFoundException("Usuário e/ou senha inválido(s)");
        }

        Collection<? extends GrantedAuthority> permissions = getPermission();

        return new SystemMedico(medico, permissions);
    }

    private Collection<? extends GrantedAuthority> getPermission() {

        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        List<String> permissions = Collections.singletonList("ROLE_DOCTOR");
        permissions.forEach(p -> authorities.add(new SimpleGrantedAuthority(p.toUpperCase())));
        return authorities;
    }
}

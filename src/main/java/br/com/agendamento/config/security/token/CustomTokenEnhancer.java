package br.com.agendamento.config.security.token;

import br.com.agendamento.domain.SystemMedico;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.HashMap;

public class CustomTokenEnhancer implements TokenEnhancer {

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {

        SystemMedico systemMedico = (SystemMedico) authentication.getPrincipal();

        HashMap<String, Object> addInfo = new HashMap<>();
        addInfo.put("medico_identifier", systemMedico.getMedico().getId());
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(addInfo);

        return accessToken;
    }
}

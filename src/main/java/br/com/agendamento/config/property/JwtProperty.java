package br.com.agendamento.config.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "agendamento.jwt")
@Data
public class JwtProperty {

    private String appClient;
    private String secret;
    private String signingKey;
    private String accessTokenValiditySeconds;
}

package br.com.agendamento.config.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;

@ConfigurationProperties(prefix = "agendamento.utils")
@Data
public class UtilsProperty {

    private String env;
    private Map<String, String> urls = new HashMap<>();
    private Map<String, String> bucketNames = new HashMap<>();
    private Map<String, String> defaultMessages = new HashMap<>();
}

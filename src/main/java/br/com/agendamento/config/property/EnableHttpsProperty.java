package br.com.agendamento.config.property;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "agendamento.security")
@Data
public class EnableHttpsProperty {

    private boolean enableHttps;
}

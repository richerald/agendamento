package br.com.agendamento.config.swagger;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
@Import(springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

    public static final String PATH = "/api/app/v1/**";
    public static final String PATH_SECURITY_REGEX = "^((\\/api\\/app\\/v1\\/(agendamentos|medicos\\/\\{id}\\/agendamentos|logout)))";
    public static final String URL_TOKEN = "http://localhost:8081/api/app/oauth/token";

    @Bean
    public Docket api() {

        Contact contact = new Contact("Richard Herald", "", "ric.herald@gmail.com");

        List<VendorExtension> vext = new ArrayList<>();
        ApiInfo apiInfo = new ApiInfo(
                "Desafio conexa",
                "Construir uma API REST onde nossos médicos de plantão consigam agendar atendimentos com pacientes.",
                "1.0",
                "",
                contact,
                "",
                "",
                vext);

        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .pathMapping("/")
                .forCodeGeneration(true)
                .genericModelSubstitutes(ResponseEntity.class)
                .securityContexts(Lists.newArrayList(securityContext()))
                .securitySchemes(Lists.newArrayList(securityScheme()))
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET, globalResponseGET())
                .globalResponseMessage(RequestMethod.DELETE, globalResponseGET())
                .globalResponseMessage(RequestMethod.POST, globalResponsePOST())
                .globalResponseMessage(RequestMethod.PUT, globalResponsePOST())
                .select()
                .paths(PathSelectors.ant(PATH))
                .apis(RequestHandlerSelectors.any())
                .build();

        return docket;
    }

    private AuthorizationScope[] scopes() {
        return new AuthorizationScope[]{};
    }

    private SecurityScheme securityScheme() {
        GrantType grant = new ResourceOwnerPasswordCredentialsGrant(URL_TOKEN);
        return new OAuthBuilder().name("OAuth2")
                .grantTypes(Collections.singletonList(grant))
                .scopes(Arrays.asList(scopes()))
                .build();
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(Collections.singletonList(new SecurityReference("OAuth2", scopes())))
                .forPaths(PathSelectors.regex(PATH_SECURITY_REGEX))
                .build();
    }

    private List<ResponseMessage> globalResponseGET()
    {
        return new ArrayList<ResponseMessage>() {{
            add(new ResponseMessageBuilder()
                    .code(500)
                    .message("Fatal error")
                    .responseModel(new ModelRef("Error"))
                    .build());
            add(new ResponseMessageBuilder()
                    .code(200)
                    .message("Ok")
                    .build());
            add(new ResponseMessageBuilder()
                    .code(404)
                    .message("Not found")
                    .build());
        }};
    }

    private List<ResponseMessage> globalResponsePOST()
    {
        return new ArrayList<ResponseMessage>() {{
            add(new ResponseMessageBuilder()
                    .code(500)
                    .message("Fatal error")
                    .responseModel(new ModelRef("Error"))
                    .build());
//            add(new ResponseMessageBuilder()
//                    .code(201)
//                    .message("Created")
//                    .build());
            add(new ResponseMessageBuilder()
                    .code(404)
                    .message("Not found")
                    .build());
            add(new ResponseMessageBuilder()
                    .code(422)
                    .message("Unprocessable Entity")
                    .build());
        }};
    }

}


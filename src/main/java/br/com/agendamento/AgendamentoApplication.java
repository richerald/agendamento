package br.com.agendamento;

import br.com.agendamento.config.property.EnableHttpsProperty;
import br.com.agendamento.config.property.JwtProperty;
import br.com.agendamento.config.property.UtilsProperty;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableConfigurationProperties({EnableHttpsProperty.class, JwtProperty.class, UtilsProperty.class})
@EnableCaching
public class AgendamentoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AgendamentoApplication.class, args);
    }
}

package br.com.agendamento.repository;

import br.com.agendamento.domain.Medico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.Optional;

@Repository
public class MedicoRepository extends GenericDao<Medico> {

    @Autowired
    protected MedicoRepository(@Qualifier("dataSource") DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected RowMapper<Medico> rowMapper() {
        return (resultSet, i) -> {

            Medico medico = new Medico();
            medico.setId(resultSet.getInt("id"));
            medico.setNome(resultSet.getString("nome"));
            medico.setSenha(resultSet.getString("senha"));
            medico.setUsuario(resultSet.getString("usuario"));
            medico.setEspecialidade(resultSet.getString("especialidade"));

            return medico;
        };
    }

    public Optional<String> getAgendamentosById(Integer id) {
        String sql = "select JSON_OBJECT( " +
                "               'medico', medico.nome, " +
                "               'especialidade', medico.especialidade, " +
                "               'agendamentos', IFNULL(( " +
                "                    select JSON_ARRAYAGG( " +
                "                        JSON_OBJECT( " +
                "                            'id', paciente.id, " +
                "                               'nome', paciente.nome, " +
                "                               'idade', paciente.idade, " +
                "                               'data', DATE_FORMAT(agendamento.agendamento_data, '%Y-%m-%d %H:%i:%S') " +
                "                            )) " +
                "                    from agendamento " +
                "                    inner join paciente on agendamento.paciente_id = paciente.id " +
                "                    where agendamento.medico_id = medico.id " +
                "                    ),JSON_ARRAY()) " +
                "           ) as JSON " +
                "from medico " +
                "where medico.id = ?";
        return super.json(sql, id);
    }

    public Optional<Medico> getById(Integer id) {

        String sql = "select id, usuario, senha, nome, especialidade from medico where id = ?";
        return super.select(sql, rowMapper(), id);
    }

    public Optional<Medico> getByUsuario(String usuario) {

        String sql = "select id, usuario, senha, nome, especialidade from medico where usuario = ?";
        return super.select(sql, rowMapper(), usuario);
    }
}

package br.com.agendamento.repository;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.List;
import java.util.Optional;

public abstract class GenericDao<T> extends JdbcDaoSupport {

    protected GenericDao(DataSource dataSource) {
        setDataSource(dataSource);
    }

    protected SqlParameterSource parameterSource(T t) {
        return null;
    }

    protected RowMapper<T> rowMapper() {
        return null;
    }

    private RowMapper<String> rowMapperJSON() {
        return (rs, i) -> rs.getString("JSON");
    }

    protected NamedParameterJdbcTemplate namedQuery() {
        return new NamedParameterJdbcTemplate(getDataSource());
    }

    protected Number save(String tableName, String id, SqlParameterSource parameterSource) {

        SimpleJdbcInsert insert = new SimpleJdbcInsert(getDataSource());
        insert.withTableName(tableName);
        insert.usingGeneratedKeyColumns(id);

        return insert.executeAndReturnKey(parameterSource);
    }

    protected Optional<T> select(String sql, RowMapper<T> rowMapper, Object... args) {
        try {
            return Optional.ofNullable(getJdbcTemplate().queryForObject(sql, rowMapper, args));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    protected boolean update(String sql, SqlParameterSource parameters) {
        int retorno = namedQuery().update(sql, parameters);
        return retorno > 0;
    }

    protected Optional<String> json(String sql) {
        try {
            List<String> array = getJdbcTemplate().query(sql, rowMapperJSON());
            return array.isEmpty() || array.get(0) == null ? Optional.empty() : Optional.of(array.get(0));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    protected Optional<String> json(String sql, Object... args) {
        try {
            List<String> array = getJdbcTemplate().query(sql, args, rowMapperJSON());
            return array.isEmpty() || array.get(0) == null ? Optional.empty() : Optional.of(array.get(0));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    protected boolean delete(String sql, Object... args) {
        int deleted = getJdbcTemplate().update(sql, args);
        return deleted > 0;
    }

    protected boolean exists(String sql, Object... obj) {
        try {
            return getJdbcTemplate().query(sql, obj, ResultSet::next);
        } catch (EmptyResultDataAccessException er) {
            return false;
        }
    }
}

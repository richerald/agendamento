package br.com.agendamento.repository;

import br.com.agendamento.domain.Paciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.Optional;

@Repository
public class PacienteRepository extends GenericDao<Paciente> {

    @Autowired
    protected PacienteRepository(@Qualifier("dataSource") DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected SqlParameterSource parameterSource(Paciente paciente) {

        return new MapSqlParameterSource()
                .addValue("id", paciente.getId())
                .addValue("nome", paciente.getNome())
                .addValue("cpf", paciente.getCpf())
                .addValue("idade", paciente.getIdade())
                .addValue("telefone", paciente.getTelefone());
    }

    @Override
    protected RowMapper<Paciente> rowMapper() {

        return (resultSet, i) -> {
            Paciente paciente = new Paciente();
            paciente.setId(resultSet.getInt("id"));
            paciente.setNome(resultSet.getString("nome"));
            paciente.setCpf(resultSet.getString("cpf"));
            paciente.setIdade(resultSet.getInt("idade"));
            paciente.setTelefone(resultSet.getString("telefone"));

            return paciente;
        };
    }

    public Paciente save(Paciente paciente) {
        Number key = super.save("paciente", "id", parameterSource(paciente));
        paciente.setId(key.intValue());
        return paciente;
    }

    public boolean delete(Integer pacienteId) {
        String sql = "delete from paciente where id = ?";
        return super.delete(sql, pacienteId);
    }

    public boolean update(Paciente paciente) {
        String sql = "update paciente set nome =:nome, idade =:idade, telefone =:telefone where id =:id;";
        return super.update(sql, parameterSource(paciente));
    }

    public Optional<Paciente> getById(Integer pacienteId) {
        String sql = "select id, nome, cpf, idade, telefone from paciente where id = ?";
        return super.select(sql, rowMapper(), pacienteId);
    }

    public Optional<String> getAll() {

        String sql = "select JSON_ARRAYAGG( " +
                "    JSON_OBJECT( " +
                "        'id', id, " +
                "        'nome', nome, " +
                "        'cpf', cpf, " +
                "        'idade', idade, " +
                "        'telefone', telefone " +
                "        )) as JSON " +
                " from paciente";
        return super.json(sql);
    }

    public boolean existsByCPF(String CPF) {
        String sql = "select 1 from paciente where cpf = ?";
        return super.exists(sql, CPF);
    }

    public boolean existsAgendamentoByPacienteId(Integer pacienteId) {
        String sql = "select 1 from agendamento where paciente_id = ?";
        return super.exists(sql, pacienteId);
    }
}

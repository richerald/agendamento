package br.com.agendamento.repository;

import br.com.agendamento.domain.Agendamento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.time.LocalDateTime;

@Repository
public class AgendamentoRepository extends GenericDao<Agendamento> {

    @Autowired
    protected AgendamentoRepository(@Qualifier("dataSource") DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected SqlParameterSource parameterSource(Agendamento agendamento) {

        return new MapSqlParameterSource()
                .addValue("id", agendamento.getId())
                .addValue("paciente_id", agendamento.getPacienteId())
                .addValue("medico_id", agendamento.getMedicoId())
                .addValue("agendamento_data", agendamento.getAgendamentoData());
    }

    public Agendamento save(Agendamento agendamento) {

        Number key = super.save("agendamento", "id", parameterSource(agendamento));
        agendamento.setId(key.intValue());
        return agendamento;
    }

    public boolean isHorarioIndisponivelMedico(LocalDateTime dataAgendamento, Integer id) {

        String sql = "select 1 " +
                " from (select id, TIMESTAMPDIFF(SECOND, agendamento_data, ?) AS minutos " +
                "      from agendamento " +
                "      where DATE(agendamento_data) = DATE(?) and medico_id = ?) ultimo_agendamento " +
                "         inner join agendamento " +
                "                    on ultimo_agendamento.id = agendamento.id " +
                " where ultimo_agendamento.minutos <= 1800 " +
                "  and ultimo_agendamento.minutos >= -1800";
        return super.exists(sql, dataAgendamento, dataAgendamento, id);
    }

    public boolean isHorarioIndisponivelPaciente(LocalDateTime dataAgendamento, Integer id) {

        String sql = "select 1 " +
                " from (select id, TIMESTAMPDIFF(SECOND, agendamento_data, ?) AS minutos " +
                "      from agendamento " +
                "      where DATE(agendamento_data) = DATE(?) and paciente_id = ?) ultimo_agendamento " +
                "         inner join agendamento " +
                "                    on ultimo_agendamento.id = agendamento.id " +
                " where ultimo_agendamento.minutos <= 1800 " +
                "  and ultimo_agendamento.minutos >= -1800";
        return super.exists(sql, dataAgendamento, dataAgendamento, id);
    }
}

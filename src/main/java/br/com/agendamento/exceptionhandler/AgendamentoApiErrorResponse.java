package br.com.agendamento.exceptionhandler;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.Collections;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;

@JsonAutoDetect(fieldVisibility = ANY)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class AgendamentoApiErrorResponse {

    private final int httpStatus;
    private final List<ApiError> errors;

    static AgendamentoApiErrorResponse of(HttpStatus status, List<ApiError> errors) {
        return new AgendamentoApiErrorResponse(status.value(), errors);
    }

    static AgendamentoApiErrorResponse of(HttpStatus status, ApiError error) {
        return of(status, Collections.singletonList(error));
    }

    @AllArgsConstructor
    @JsonAutoDetect(fieldVisibility = ANY)
    static class ApiError {

        private final String code;
        private final String userMessage;
    }
}

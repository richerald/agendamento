package br.com.agendamento.exceptionhandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Locale;

@Order(Ordered.LOWEST_PRECEDENCE)
@RestControllerAdvice
public class GeneralExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneralExceptionHandler.class);
    private static final String ERROR_1 = "error-1";

    private final AgendamentoExceptionHandler agendamentoExceptionHandler;

    @Autowired
    public GeneralExceptionHandler(AgendamentoExceptionHandler agendamentoExceptionHandler) {
        this.agendamentoExceptionHandler = agendamentoExceptionHandler;
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<AgendamentoApiErrorResponse> handlerInternalServerError(Exception e, Locale locale) {

        LOGGER.error("Erro não esperado: " + e.getMessage());

        e.printStackTrace();

        final HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        final AgendamentoApiErrorResponse agendamentoApiErrorResponse = AgendamentoApiErrorResponse.of(status, agendamentoExceptionHandler.toApiError(ERROR_1, locale));
        return ResponseEntity.status(status).body(agendamentoApiErrorResponse);
    }
}

package br.com.agendamento.exceptionhandler;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class AgendamentoExceptionHandler {

    private static final String NO_MESSAGE_AVAILABLE = "No message available";
    private static final String GENERIC_1 = "generic-1";
    private static final String GENERIC_ERROR_2 = "generic-error-2";
    private static final String INVALID_PARAMETER = "invalid-parameter";

    private final MessageSource apiErrorMessageSource;

    public AgendamentoExceptionHandler(MessageSource apiErrorMessageSource) {
        this.apiErrorMessageSource = apiErrorMessageSource;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<AgendamentoApiErrorResponse> handlerNotValidException(MethodArgumentNotValidException e, Locale locale) {

        Stream<ObjectError> errorStream = e.getBindingResult().getAllErrors().stream();
        List<AgendamentoApiErrorResponse.ApiError> apiErrors = errorStream
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .map(defaultMessage -> toApiError(defaultMessage, locale))
                .collect(Collectors.toList());
        AgendamentoApiErrorResponse errorResponse = AgendamentoApiErrorResponse.of(BAD_REQUEST, apiErrors);
        return ResponseEntity.badRequest().body(errorResponse);
    }

    @ExceptionHandler(InvalidFormatException.class)
    public ResponseEntity<AgendamentoApiErrorResponse> handlerInvalidFormatException(InvalidFormatException e, Locale locale) {

        final AgendamentoApiErrorResponse errorResponse = AgendamentoApiErrorResponse.of(BAD_REQUEST, toApiError(GENERIC_1, locale, e.getValue()));
        return ResponseEntity.badRequest().body(errorResponse);
    }

    @ExceptionHandler({BusinessException.class})
    public ResponseEntity<AgendamentoApiErrorResponse> handleBusinessException(BusinessException e, Locale locale) {

        final AgendamentoApiErrorResponse agendamentoApiErrorResponse = AgendamentoApiErrorResponse.of(e.getStatus(), toApiError(e.getErrorCode(), locale));
        return ResponseEntity.status(e.getStatus()).body(agendamentoApiErrorResponse);
    }

    @ExceptionHandler({MissingServletRequestParameterException.class})
    public ResponseEntity<AgendamentoApiErrorResponse> handleMissingRequestParameterException(MissingServletRequestParameterException e) {

        final AgendamentoApiErrorResponse agendamentoApiErrorResponse = AgendamentoApiErrorResponse.of(
                BAD_REQUEST, toApiErrorWithOwnMessage(INVALID_PARAMETER, e.getMessage()));

        return ResponseEntity.status(BAD_REQUEST).body(agendamentoApiErrorResponse);
    }

    @ExceptionHandler({DataAccessException.class})
    public ResponseEntity<AgendamentoApiErrorResponse> handleDataAccessExceptionException(DataAccessException e, Locale locale) {

        final AgendamentoApiErrorResponse agendamentoApiErrorResponse = AgendamentoApiErrorResponse.of(
                INTERNAL_SERVER_ERROR, toApiError(GENERIC_ERROR_2, locale));
        return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(agendamentoApiErrorResponse);
    }

    public AgendamentoApiErrorResponse.ApiError toApiError(String code, Locale locale, Object... args) {

        String message;

        try {
            message = apiErrorMessageSource.getMessage(code, args, locale);
        } catch (NoSuchMessageException e) {
            message = NO_MESSAGE_AVAILABLE;
        }

        return new AgendamentoApiErrorResponse.ApiError(code, message);
    }

    public AgendamentoApiErrorResponse.ApiError toApiErrorWithOwnMessage(String code, String message) {
        try {
            return new AgendamentoApiErrorResponse.ApiError(code, message);
        } catch (NoSuchMessageException e) {
            return new AgendamentoApiErrorResponse.ApiError(code, NO_MESSAGE_AVAILABLE);
        }
    }
}
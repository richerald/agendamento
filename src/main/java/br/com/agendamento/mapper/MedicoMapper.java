package br.com.agendamento.mapper;

import br.com.agendamento.domain.Medico;
import br.com.agendamento.dto.MedicoDTO;
import org.springframework.stereotype.Service;

@Service
public class MedicoMapper {

    public MedicoDTO toDTO(Medico medico) {

        MedicoDTO medicoDTO = new MedicoDTO();
        medicoDTO.setId(medico.getId());
        medicoDTO.setNome(medico.getNome());
        medicoDTO.setUsuario(medicoDTO.getUsuario());
        medicoDTO.setSenha(medicoDTO.getSenha());
        medicoDTO.setEspecialidade(medicoDTO.getEspecialidade());

        return medicoDTO;
    }
}

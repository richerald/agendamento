package br.com.agendamento.mapper;

import br.com.agendamento.dto.PacienteDTO;
import br.com.agendamento.domain.Paciente;
import org.springframework.stereotype.Service;

@Service
public class PacienteMapper {

    public PacienteDTO toDTO(Paciente paciente) {

        PacienteDTO pacienteDTO = new PacienteDTO();
        pacienteDTO.setId(paciente.getId());
        pacienteDTO.setCpf(paciente.getCpf());
        pacienteDTO.setIdade(paciente.getIdade());
        pacienteDTO.setNome(paciente.getNome());
        pacienteDTO.setTelefone(paciente.getTelefone());

        return pacienteDTO;
    }

    public Paciente toEntity(PacienteDTO pacienteDTO) {

        Paciente paciente = new Paciente();
        paciente.setId(pacienteDTO.getId());
        paciente.setCpf(pacienteDTO.getCpf());
        paciente.setIdade(pacienteDTO.getIdade());
        paciente.setNome(pacienteDTO.getNome());
        paciente.setTelefone(pacienteDTO.getTelefone());

        return paciente;
    }
}

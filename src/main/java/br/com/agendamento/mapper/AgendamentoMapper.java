package br.com.agendamento.mapper;

import br.com.agendamento.domain.Agendamento;
import br.com.agendamento.dto.AgendamentoDTO;
import org.springframework.stereotype.Service;

@Service
public class AgendamentoMapper {

    public Agendamento toEntity(AgendamentoDTO agendamentoDTO) {

        Agendamento agendamento = new Agendamento();
        agendamento.setId(agendamentoDTO.getId());
        agendamento.setPacienteId(agendamentoDTO.getPacienteId());
        agendamento.setMedicoId(agendamentoDTO.getMedicoId());
        agendamento.setAgendamentoData(agendamentoDTO.getAgendamentoData());

        return agendamento;
    }
}

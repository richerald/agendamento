package br.com.agendamento.utils;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;

@Component
public class LocalDateTimeUtils {

    public LocalDateTime getLocalDateTime() {
        return LocalDateTime.now(ZoneId.of("America/Bahia"));
    }

    public boolean isAfterNow(LocalDateTime date) {
        return getLocalDateTime().compareTo(date) >= 0;
    }
}

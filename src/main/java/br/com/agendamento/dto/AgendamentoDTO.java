package br.com.agendamento.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.OptBoolean;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class AgendamentoDTO {

    private Integer id;

    @NotNull(message = "agendamento-dto.paciente-id-required")
    private Integer pacienteId;

    private Integer medicoId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "America/Bahia")
    @NotNull(message = "agendamento-dto.agendamento-data-required")
    private LocalDateTime agendamentoData;
}

package br.com.agendamento.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PacienteDTO {

    private Integer id;

    @NotBlank(message = "paciente-dto.nome-required")
    private String nome;

    @NotBlank(message = "paciente-dto.cpf-required")
    @Size(max = 11, message = "paciente-dto.cpf-max-lenght")
    private String cpf;

    @NotNull(message = "paciente-dto.idade-required")
    @Range(min = 1, max = 999, message = "paciente-dto.idade-max-lenght")
    private Integer idade;

    @NotBlank(message = "paciente-dto.telefone-required")
    @Pattern(regexp = "\\d{10,11}", message = "paciente-dto.telephone-invalid")
    private String telefone;
}

package br.com.agendamento.dto;

import lombok.Data;

@Data
public class MedicoDTO {

    private Integer id;
    private String nome;
    private String usuario;
    private String senha;
    private String especialidade;
}

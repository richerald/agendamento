package br.com.agendamento.domain;

import lombok.Data;

@Data
public class Medico {

    private Integer id;
    private String nome;
    private String usuario;
    private String senha;
    private String especialidade;
}

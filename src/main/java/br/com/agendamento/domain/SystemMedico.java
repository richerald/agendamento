package br.com.agendamento.domain;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class SystemMedico extends User {

    private static final long serialVersionUID = 8232147438276172895L;

    private final Medico medico;

    public SystemMedico(Medico medico, Collection<? extends GrantedAuthority> authorities) {
        super(medico.getNome(), medico.getSenha(), authorities);
        this.medico = medico;
    }

    public Medico getMedico() {
        return medico;
    }
}

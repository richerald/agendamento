package br.com.agendamento.domain;

import lombok.Data;

@Data
public class Paciente {

    private Integer id;
    private String nome;
    private String cpf;
    private Integer idade;
    private String telefone;
}

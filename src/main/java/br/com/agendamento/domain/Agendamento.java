package br.com.agendamento.domain;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Agendamento {

    private Integer id;
    private Integer pacienteId;
    private Integer medicoId;
    private LocalDateTime agendamentoData;
}

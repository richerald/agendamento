create TABLE IF NOT EXISTS paciente
(
    id       INT(11) AUTO_INCREMENT PRIMARY KEY,
    nome     VARCHAR(255) NOT NULL,
    cpf      VARCHAR(11)  NOT NULL UNIQUE,
    idade    INT          NOT NULL,
    telefone VARCHAR(12)  NOT NULL
) ENGINE = INNODB;
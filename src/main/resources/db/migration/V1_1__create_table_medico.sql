create TABLE IF NOT EXISTS medico
(
    id              INT(11) AUTO_INCREMENT PRIMARY KEY,
    usuario         VARCHAR(100) NOT NULL UNIQUE,
    senha           VARCHAR(64) NOT NULL,
    nome            VARCHAR(255) NOT NULL,
    especialidade   VARCHAR(100) NOT NULL
) ENGINE = INNODB;

INSERT INTO medico (usuario, senha, nome, especialidade)
VALUES ('eraldo.neto', '$2a$10$Y0Gow85yZEya1YJdOofk6.f/zfk3r3l.lj/.GRpNI0yoKXpG.NBFy', 'Eraldo Neto', 'Urologista');

INSERT INTO medico (usuario, senha, nome, especialidade)
VALUES ('richard.herald', '$2a$10$Y0Gow85yZEya1YJdOofk6.f/zfk3r3l.lj/.GRpNI0yoKXpG.NBFy', 'Richard Herald',
        'Oftalmologista');

INSERT INTO paciente (nome, cpf, idade, telefone)
VALUES ('Ygor Cesar', '12345678909', '30', '79998604433');

create TABLE IF NOT EXISTS agendamento
(
    id               INT(11) AUTO_INCREMENT PRIMARY KEY,
    agendamento_data DATETIME NOT NULL,
    paciente_id      INT(11)  NOT NULL,
    medico_id        INT(11)  NOT NULL,

    FOREIGN KEY (paciente_id) REFERENCES paciente (id),
    FOREIGN KEY (medico_id) REFERENCES medico (id)
) ENGINE = INNODB;
package br.com.agendamento.service;

import br.com.agendamento.MockitoExtension;
import br.com.agendamento.domain.Paciente;
import br.com.agendamento.dto.PacienteDTO;
import br.com.agendamento.exceptionhandler.BusinessException;
import br.com.agendamento.mapper.PacienteMapper;
import br.com.agendamento.repository.PacienteRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PacienteServiceImplTest {

    @InjectMocks
    private PacienteServiceImpl pacienteService;

    @Mock
    private PacienteRepository pacienteRepository;

    @Mock
    private PacienteMapper pacienteMapper;

    @Nested
    @DisplayName("Deve testar a busca de um paciente")
    class findById {

        @Test
        @DisplayName("Deve testar a busca de um paciente com sucesso")
        void findByIdSuccess() {

            Paciente paciente = new Paciente();
            paciente.setId(1);
            paciente.setNome("richard");
            paciente.setIdade(25);
            paciente.setTelefone("79998605744");
            paciente.setCpf("05598732145");

            PacienteDTO pacienteDTO = new PacienteDTO();
            pacienteDTO.setId(1);
            pacienteDTO.setNome("richard");
            pacienteDTO.setIdade(25);
            pacienteDTO.setTelefone("79998605744");
            pacienteDTO.setCpf("05598732145");

            when(pacienteRepository.getById(pacienteDTO.getId())).thenReturn(Optional.of(paciente));
            when(pacienteMapper.toDTO(paciente)).thenReturn(pacienteDTO);

            PacienteDTO response = pacienteService.getById(pacienteDTO.getId());

            assertEquals(response.getId(), pacienteDTO.getId());
            assertEquals(response.getNome(), pacienteDTO.getNome());
            assertEquals(response.getIdade(), pacienteDTO.getIdade());
            assertEquals(response.getTelefone(), pacienteDTO.getTelefone());
            assertEquals(response.getCpf(), pacienteDTO.getCpf());

            verify(pacienteRepository, times(1)).getById(pacienteDTO.getId());
            verify(pacienteMapper,times(1)).toDTO(paciente);
        }

        @Test
        @DisplayName("Deve lancar um erro na busca de um paciente pelo id")
        void findByIdNotFound() {

            Paciente paciente = new Paciente();
            paciente.setId(1);
            paciente.setNome("richard");
            paciente.setIdade(25);
            paciente.setTelefone("79998605744");
            paciente.setCpf("05598732145");

            PacienteDTO pacienteDTO = new PacienteDTO();
            pacienteDTO.setId(1);
            pacienteDTO.setNome("richard");
            pacienteDTO.setIdade(25);
            pacienteDTO.setTelefone("79998605744");
            pacienteDTO.setCpf("05598732145");

            when(pacienteRepository.getById(pacienteDTO.getId())).thenReturn(Optional.empty());

            BusinessException businessException = assertThrows(BusinessException.class, () -> pacienteService.getById(pacienteDTO.getId()));

            assertEquals(HttpStatus.NOT_FOUND, businessException.getStatus());
            assertEquals("paciente.not-found", businessException.getErrorCode());

            verify(pacienteRepository, times(1)).getById(pacienteDTO.getId());
            verify(pacienteMapper,times(0)).toDTO(paciente);
        }
    }
}

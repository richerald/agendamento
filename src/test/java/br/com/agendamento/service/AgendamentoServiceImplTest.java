package br.com.agendamento.service;

import br.com.agendamento.MockitoExtension;
import br.com.agendamento.domain.Agendamento;
import br.com.agendamento.dto.AgendamentoDTO;
import br.com.agendamento.dto.MedicoDTO;
import br.com.agendamento.dto.PacienteDTO;
import br.com.agendamento.exceptionhandler.BusinessException;
import br.com.agendamento.mapper.AgendamentoMapper;
import br.com.agendamento.repository.AgendamentoRepository;
import br.com.agendamento.utils.LocalDateTimeUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AgendamentoServiceImplTest {

    @InjectMocks
    private AgendamentoServiceImpl agendamentoService;

    @Mock
    private AgendamentoRepository agendamentoRepository;

    @Mock
    private PacienteService pacienteService;

    @Mock
    private MedicoService medicoService;

    @Mock
    private MedicoResolver medicoResolver;

    @Mock
    private AgendamentoMapper agendamentoMapper;

    @Mock
    private LocalDateTimeUtils localDateTimeUtils;


    @Nested
    @DisplayName("Deve testar salvar um agendamento")
    class save {

        private PacienteDTO pacienteDTO;
        private MedicoDTO medicoDTO;
        private Agendamento agendamento;
        private AgendamentoDTO agendamentoDTO;

        @BeforeEach
        void setup() {
            pacienteDTO = new PacienteDTO();
            pacienteDTO.setId(1);
            pacienteDTO.setNome("richard");
            pacienteDTO.setTelefone("79998604455");
            pacienteDTO.setIdade(25);
            pacienteDTO.setCpf("05598745653");

            medicoDTO = new MedicoDTO();
            medicoDTO.setId(1);
            medicoDTO.setUsuario("eraldo.neto");
            medicoDTO.setNome("Eraldo");
            medicoDTO.setEspecialidade("Urologista");

            agendamento = new Agendamento();
            agendamento.setId(1);
            agendamento.setPacienteId(1);
            agendamento.setMedicoId(1);
            agendamento.setAgendamentoData(localDateTimeUtils.getLocalDateTime());

            agendamentoDTO = new AgendamentoDTO();
            agendamentoDTO.setAgendamentoData(LocalDateTime.of(2021, 5, 1, 10, 0, 0));
            agendamentoDTO.setPacienteId(1);
            agendamentoDTO.setMedicoId(1);
        }

        @Test
        @DisplayName("deve salvar um agendamento com sucesso")
        void saveSuccess() {
            when(localDateTimeUtils.getLocalDateTime()).thenReturn(LocalDateTime.of(2021, 3, 2, 10, 0, 0));
            when(localDateTimeUtils.isAfterNow(agendamentoDTO.getAgendamentoData())).thenReturn(false);
            when(medicoResolver.getMedicoId()).thenReturn(agendamentoDTO.getMedicoId());
            when(pacienteService.getById(anyInt())).thenReturn(pacienteDTO);
            when(medicoService.getById(anyInt())).thenReturn(medicoDTO);
            when(agendamentoRepository.isHorarioIndisponivelMedico(agendamentoDTO.getAgendamentoData(), agendamento.getMedicoId())).thenReturn(false);
            when(agendamentoRepository.isHorarioIndisponivelPaciente(agendamentoDTO.getAgendamentoData(), agendamento.getPacienteId())).thenReturn(false);
            when(agendamentoMapper.toEntity(agendamentoDTO)).thenReturn(agendamento);
            when(agendamentoRepository.save(agendamento)).thenReturn(agendamento);

            AgendamentoDTO response = agendamentoService.save(agendamentoDTO);

            assertEquals(response.getMedicoId(), agendamento.getMedicoId());
            assertEquals(response.getPacienteId(), agendamento.getPacienteId());

            verify(localDateTimeUtils, times(1)).getLocalDateTime();
            verify(localDateTimeUtils, times(1)).isAfterNow(agendamentoDTO.getAgendamentoData());
            verify(pacienteService, times(1)).getById(agendamentoDTO.getPacienteId());
            verify(medicoService, times(1)).getById(agendamentoDTO.getMedicoId());
            verify(agendamentoRepository, times(1)).isHorarioIndisponivelPaciente(agendamentoDTO.getAgendamentoData(), agendamentoDTO.getPacienteId());
            verify(agendamentoRepository, times(1)).isHorarioIndisponivelMedico(agendamentoDTO.getAgendamentoData(), agendamentoDTO.getMedicoId());
            verify(agendamentoMapper, times(1)).toEntity(agendamentoDTO);
            verify(agendamentoRepository, times(1)).save(agendamento);
        }

        @Test
        @DisplayName("Deve lançar erro ao validar a data do agendamento")
        void errorWhenIsDateAfterNow() {
            when(localDateTimeUtils.getLocalDateTime()).thenReturn(LocalDateTime.of(2021, 6, 2, 10, 0, 0));
            when(localDateTimeUtils.isAfterNow(agendamentoDTO.getAgendamentoData())).thenReturn(true);

            BusinessException businessException = assertThrows(BusinessException.class, () -> agendamentoService.save(agendamentoDTO));

            assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, businessException.getStatus());
            assertEquals("agendamento.agendamento-data-in-the-past", businessException.getErrorCode());

            verify(localDateTimeUtils, times(1)).getLocalDateTime();
            verify(localDateTimeUtils, times(1)).isAfterNow(agendamentoDTO.getAgendamentoData());
            verify(pacienteService, times(0)).getById(agendamentoDTO.getPacienteId());
            verify(medicoService, times(0)).getById(agendamentoDTO.getMedicoId());
            verify(agendamentoRepository, times(0)).isHorarioIndisponivelPaciente(agendamentoDTO.getAgendamentoData(), agendamentoDTO.getPacienteId());
            verify(agendamentoRepository, times(0)).isHorarioIndisponivelMedico(agendamentoDTO.getAgendamentoData(), agendamentoDTO.getMedicoId());
            verify(agendamentoMapper, times(0)).toEntity(agendamentoDTO);
            verify(agendamentoRepository, times(0)).save(agendamento);
        }

        @Test
        @DisplayName("Deve lançar erro ao validar se o medico esta com horario disponivel para agendamento")
        void errorWhenHorarioIndisponivelMedico() {
            when(localDateTimeUtils.getLocalDateTime()).thenReturn(LocalDateTime.of(2021, 3, 2, 10, 0, 0));
            when(localDateTimeUtils.isAfterNow(agendamentoDTO.getAgendamentoData())).thenReturn(false);
            when(medicoResolver.getMedicoId()).thenReturn(agendamentoDTO.getMedicoId());
            when(pacienteService.getById(anyInt())).thenReturn(pacienteDTO);
            when(medicoService.getById(anyInt())).thenReturn(medicoDTO);
            when(agendamentoRepository.isHorarioIndisponivelPaciente(agendamentoDTO.getAgendamentoData(), agendamento.getMedicoId())).thenReturn(true);

            BusinessException businessException = assertThrows(BusinessException.class, () -> agendamentoService.save(agendamentoDTO));

            assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, businessException.getStatus());
            assertEquals("agendamento.paciente-no-time-available", businessException.getErrorCode());

            verify(localDateTimeUtils, times(1)).getLocalDateTime();
            verify(localDateTimeUtils, times(1)).isAfterNow(agendamentoDTO.getAgendamentoData());
            verify(pacienteService, times(1)).getById(agendamentoDTO.getPacienteId());
            verify(medicoService, times(1)).getById(agendamentoDTO.getMedicoId());
            verify(agendamentoRepository, times(1)).isHorarioIndisponivelPaciente(agendamentoDTO.getAgendamentoData(), agendamentoDTO.getPacienteId());
            verify(agendamentoRepository, times(0)).isHorarioIndisponivelMedico(agendamentoDTO.getAgendamentoData(), agendamentoDTO.getMedicoId());
            verify(agendamentoMapper, times(0)).toEntity(agendamentoDTO);
            verify(agendamentoRepository, times(0)).save(agendamento);
        }

        @Test
        @DisplayName("Deve lançar erro ao validar se o paciente esta com horario disponivel para agendamento")
        void errorWhenHorarioIndisponivelPaciente() {
            when(localDateTimeUtils.getLocalDateTime()).thenReturn(LocalDateTime.of(2021, 3, 2, 10, 0, 0));
            when(localDateTimeUtils.isAfterNow(agendamentoDTO.getAgendamentoData())).thenReturn(false);
            when(medicoResolver.getMedicoId()).thenReturn(agendamentoDTO.getMedicoId());
            when(pacienteService.getById(anyInt())).thenReturn(pacienteDTO);
            when(medicoService.getById(anyInt())).thenReturn(medicoDTO);
            when(agendamentoRepository.isHorarioIndisponivelPaciente(agendamentoDTO.getAgendamentoData(), agendamento.getMedicoId())).thenReturn(false);
            when(agendamentoRepository.isHorarioIndisponivelMedico(agendamentoDTO.getAgendamentoData(), agendamento.getMedicoId())).thenReturn(true);

            BusinessException businessException = assertThrows(BusinessException.class, () -> agendamentoService.save(agendamentoDTO));

            assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, businessException.getStatus());
            assertEquals("agendamento.medico-no-time-available", businessException.getErrorCode());

            verify(localDateTimeUtils, times(1)).getLocalDateTime();
            verify(localDateTimeUtils, times(1)).isAfterNow(agendamentoDTO.getAgendamentoData());
            verify(pacienteService, times(1)).getById(agendamentoDTO.getPacienteId());
            verify(medicoService, times(1)).getById(agendamentoDTO.getMedicoId());
            verify(agendamentoRepository, times(1)).isHorarioIndisponivelPaciente(agendamentoDTO.getAgendamentoData(), agendamentoDTO.getPacienteId());
            verify(agendamentoRepository, times(1)).isHorarioIndisponivelMedico(agendamentoDTO.getAgendamentoData(), agendamentoDTO.getMedicoId());
            verify(agendamentoMapper, times(0)).toEntity(agendamentoDTO);
            verify(agendamentoRepository, times(0)).save(agendamento);
        }
    }
}

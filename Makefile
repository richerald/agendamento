.PHONY: up
up:
	docker-compose up -d mysql redis
	sleep 3
	./mvnw clean package -DskipTests
	docker build -t agendamento:v1 .
	docker-compose up agendamento

.PHONY: down
down:
	docker-compose down